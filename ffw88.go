package main

import (
	"crypto/sha256"
	"crypto/subtle"
	"database/sql"
	"log"
	"net/http"
	"net/url"
	"strings"
	"fmt"
	"io/ioutil"

	_ "github.com/mattn/go-sqlite3"
)

func handleError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

type user struct {
	name  string
	admin bool
}

func authenticate(db *sql.DB, w http.ResponseWriter, r *http.Request) *user {
	username, password, ok := r.BasicAuth()

	if !ok {
		w.Header().Add("WWW-Authenticate", `Basic realm="ffw88"`)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return nil
	}

	row := db.QueryRow("SELECT pass, admin FROM user WHERE name = ?;", username)
	var expectedPasswordHash []byte
	var admin bool
	err := row.Scan(&expectedPasswordHash, &admin)

	passwordHash := sha256.Sum256([]byte(password))
	passwordMatch := subtle.ConstantTimeCompare(passwordHash[:], expectedPasswordHash[:]) == 1

	if err == sql.ErrNoRows || !passwordMatch {
		w.Header().Add("WWW-Authenticate", `Basic realm="ffw88"`)
		http.Error(w, "Invalid credentials", http.StatusUnauthorized)
		return nil
	}

	return &user{username, admin}
}

func addUser(db *sql.DB, username string, pass string, admin bool) error {
	passwordHash := sha256.Sum256([]byte(pass))
	_, err := db.Exec("INSERT INTO user (name, pass, admin) VALUES (?,?,?);", username, passwordHash[:], admin)
	return err
}

func logRequest(db *sql.DB, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		res, err := db.Exec(`INSERT INTO request (
			timestamp, method, url,
			proto, proto_major, proto_minor,
			content_length, host, remote_addr)
			VALUES (CURRENT_TIMESTAMP,?,?,?,?,?,?,?,?);`,
			r.Method, r.URL.String(),
			r.Proto, r.ProtoMajor, r.ProtoMinor,
			r.ContentLength, r.Host, r.RemoteAddr)
		handleError(err)
		request_id, err := res.LastInsertId()
		handleError(err)
		for header, values := range r.Header {
			for _, value := range values {
				_, err := db.Exec("INSERT INTO header (request_id, key, value) VALUES (?,?,?);",
					request_id, header, value)
				handleError(err)
			}
		}
		for key, values := range r.Form {
			for _, value := range values {
				_, err := db.Exec("INSERT INTO form (request_id, key, value) VALUES (?,?,?);",
					request_id, key, value)
				handleError(err)
			}
		}
		next.ServeHTTP(w, r)
	})
}

func getLink(db *sql.DB, slug string) (url string, user string, title string, desc string, image string, ok bool) {
	log.Println("Getting link to", slug)
	row := db.QueryRow("SELECT url, user, title, description, image FROM link WHERE slug = ?;", slug)
	var mtitle sql.NullString
	var mdesc sql.NullString
	var mimage sql.NullString
	err := row.Scan(&url, &user, &mtitle, &mdesc, &mimage)
	if err == sql.ErrNoRows {
		return "", "", "", "", "", false
	}
	handleError(err)
	return url, user, mtitle.String, mdesc.String, mimage.String, true
}

func setLink(db *sql.DB, slug string, url *url.URL, title string, desc string, image string, user *user) {
	log.Println("Setting link from", slug, "to", url)
	_, err := db.Exec("INSERT OR REPLACE INTO link (slug, url, title, description, image, user) VALUES (?,?,?,?,?,?);", slug, url.String(), title, desc, image, user.name)
	if err != nil {
		log.Fatal(err)
	}
}

func delLink(db *sql.DB, slug string) {
	log.Println("Deleting link to", slug)
	_, err := db.Exec("DELETE FROM link WHERE slug = ?;", slug)
	if err != nil {
		log.Fatal(err)
	}
}

func checkScope(db *sql.DB, user *user, slug string) (scope bool) {
	row := db.QueryRow("SELECT EXISTS (SELECT * FROM scope WHERE user = ? AND ? LIKE scope || '%');", user.name, slug)
	err := row.Scan(&scope)
	if err != nil {
		log.Fatal(err)
	}
	return scope
}

func handler(db *sql.DB) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		slug := strings.TrimRight(req.URL.Path, "/")

		if req.FormValue("_method") != "" {
			req.Method = strings.ToUpper(req.FormValue("_method"))
		}

		var user *user
		if req.Method != http.MethodGet {
			user = authenticate(db, w, req)
			if user == nil {
				return
			}
			_, username, _, _, _, ok := getLink(db, slug)
			if ok && user.name != username && !user.admin {
				http.Error(w, "Unauthorized: not your short link", http.StatusUnauthorized)
				return
			}
			if !ok && !user.admin && !checkScope(db, user, slug) {
				http.Error(w, "Unauthorized: not part of your scope", http.StatusUnauthorized)
				return
			}
		}

		switch req.Method {

		case http.MethodGet:
			url, _, title, desc, image, ok := getLink(db, slug)
			if !ok {
				http.ServeFile(w, req, "create.html")
			} else {
				if strings.HasPrefix(req.UserAgent(), "Mozilla") {
					http.Redirect(w, req, url, http.StatusFound)
				} else {
					fmt.Fprint(w, "<!DOCTYPE html><html><head>")
					// fmt.Fprint(w, "<meta property=\"og:type\" content=\"website\">")
					// fmt.Fprint(w, "<meta property=\"og:site_name\" content=\"ffw88.nl\">")
					// fmt.Fprintf(w, "<meta property=\"og:url\" content=\"%s\">", url)
					if len(title) > 0 {
						fmt.Fprintf(w, "<meta property=\"og:title\" content=\"%s\">", title)
						// fmt.Fprintf(w, "<meta property=\"twitter:title\" content=\"%s\">", title)
					}
					if len(desc) > 0 {
						// fmt.Fprintf(w, "<meta property=\"description\" content=\"%s\">", desc)
						fmt.Fprintf(w, "<meta property=\"og:description\" content=\"%s\">", desc)
						// fmt.Fprintf(w, "<meta property=\"twitter:description\" content=\"%s\">", desc)
					}
					if len(image) > 0 {
						fmt.Fprintf(w, "<meta property=\"og:image\" content=\"%s\">", image)
						// fmt.Fprintf(w, "<meta property=\"twitter:image\" content=\"%s\">", image)
						// fmt.Fprint(w, "<meta property=\"twitter:card\" content=\"summary_large_image\">")
					}
					fmt.Fprintf(w, "<meta http-equiv=\"refresh\" content=\"0; url=%s\">", url)
					fmt.Fprint(w, "</head></html>")
				}
			}

		case http.MethodPut:
			dest, err := url.ParseRequestURI(req.FormValue("url"))
			if err != nil {
				http.Error(w, "Invalid url: "+err.Error(), http.StatusBadRequest)
				return
			}
			title := req.FormValue("title")
			desc := req.FormValue("description")
			image :=req.FormValue("image")
			setLink(db, slug, dest, title, desc, image, user)
			http.Error(w, "The short link is ready!", http.StatusCreated)

		case http.MethodDelete:
			delLink(db, slug)
			http.Error(w, "The short link was deleted", http.StatusAccepted)

		default:
			http.Error(w, "Bad request", http.StatusBadRequest)
		}
	})
}

func searchHandler(w http.ResponseWriter, r *http.Request) {
	apiKey := "40998978-418d32d50f8d29f5610015dc6"
	query := r.URL.Query().Get("q")
	url := fmt.Sprintf("https://pixabay.com/api?key=%s&q=%s", apiKey, query)

	response, err := http.Get(url)
	if err != nil {
		http.Error(w, "Failed to fetch data from Pixabay", http.StatusInternalServerError)
		return
	}
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		http.Error(w, "Failed to read response from Pixabay", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(body)
}

func setupDatabase() *sql.DB {
	db, err := sql.Open("sqlite3", "./ffw88.db")
	if err != nil {
		log.Fatalln("Could not open database ./ffw88.db", err)
	}
	db.Exec(`CREATE TABLE user (
		name TEXT PRIMARY KEY,
		pass BLOB NOT NULL,
		admin BOOLEAN NOT NULL
		);`)
	db.Exec(`CREATE TABLE scope (
		id INTEGER PRIMARY KEY,
		user TEXT NOT NULL,
		scope TEXT NOT NULL,
		FOREIGN KEY (user) REFERENCES user(name)
		)`)
	db.Exec(`CREATE TABLE link (
		slug TEXT PRIMARY KEY,
		url TEXT NOT NULL,
		title TEXT,
		description TEXT,
		image TEXT,
		user TEXT NOT NULL,
		FOREIGN KEY (user) REFERENCES user(name)
		);`)
	db.Exec(`CREATE TABLE request (
		id INTEGER PRIMARY KEY,
		timestamp TIMESTAMP,
		method TEXT,
		url TEXT,
		proto TEXT,
		proto_major INTEGER,
		proto_minor INTEGER,
		content_length INTEGER,
		host TEXT,
		remote_addr TEXT,
		request_uri TEXT
		);`)
	db.Exec(`CREATE TABLE header (
		id INTEGER PRIMARY KEY,
		request_id INTEGER,
		key TEXT,
		value TEXT,
		FOREIGN KEY (request_id) REFERENCES request(id)
		);`)
	db.Exec(`CREATE TABLE form (
		id INTEGER PRIMARY KEY,
		request_id INTEGER,
		key TEXT,
		value TEXT,
		FOREIGN KEY (request_id) REFERENCES request(id)
		);`)
	return db
}

func main() {
	db := setupDatabase()
	defer db.Close()

	http.HandleFunc("/api/search", searchHandler)

	http.Handle("/", logRequest(db, handler(db)))
	http.ListenAndServe("localhost:8090", nil)
}
